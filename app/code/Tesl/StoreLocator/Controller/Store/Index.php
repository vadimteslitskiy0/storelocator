<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Controller\Store;

use Magento\Framework\App\RequestInterface as Request;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private RedirectFactory $redirectFactory;

    /**
     * @param PageFactory $pageFactory
     * @param ForwardFactory $forward
     * @param Request $request
     */
    public function __construct(
        PageFactory $pageFactory,
        ForwardFactory $forward,
        Request $request,
        RedirectFactory $redirectFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->forward = $forward;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $page = $this->pageFactory->create();
        $store = $this->request->getParam('store');

        if (!$store) {
            return $this->forward->create()->setController('index')->forward('index');
        }
        if ($store->getData() === null) {
            $page->getConfig()->getTitle()->prepend(__('No such store'));
            return $page;
        }

        $name = $store->getName();
        $page->setHeader('name', $name);
        $page->getConfig()->getTitle()->prepend($name);
        return $page;

    }
}
