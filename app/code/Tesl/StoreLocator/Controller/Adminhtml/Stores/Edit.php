<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Controller\Adminhtml\Stores;

use Tesl\StoreLocator\Model\Authorization;
use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterfaceFactory as StoreLocatorFactory;
use Tesl\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Edit extends Action implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = Authorization::ACTION_STORE_LOCATOR_EDIT;
    /**
     * @api
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     * @param Context $context
     */
    public function __construct(
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource,
        Context $context
    ) {
        parent::__construct($context);
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
    }

    /**
     * Edit
     *
     * @return ResultInterface
     */
    public function execute() : \Magento\Framework\Controller\ResultInterface
    {
        $storeLocator = $this->storeLocatorFactory->create();

        if ($entityId = (int) $this->getRequest()->getParam('entity_id')) {
            $this->storeLocatorResource->load($storeLocator, $entityId);

            if (!$storeLocator->getId()) {
                $this->messageManager->addErrorMessage(__('This store no exists'));

                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(
            $storeLocator->getId() ? __('Edit Store') : __('New Store')
        );

        return $resultPage;
    }
}
