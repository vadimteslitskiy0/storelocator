<?php

declare(strict_types=1);

namespace Tesl\StoreLocator\Controller\Adminhtml\Stores;

use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface as StoreLocator;
use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterfaceFactory as StoreLocatorFactory;
use Tesl\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Tesl\StoreLocator\Api\StoreLocatorRepositoryInterface as StoreLocatorRepository;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException as AlreadyExistsExceptionAlias;
use Magento\Framework\Exception\LocalizedException;
use Tesl\StoreLocator\Model\Geolocation;

/**
 * Save the data
 *
 * Class Save
 */
class Save extends Action implements HttpPostActionInterface
{
    /**
     * @param StoreLocatorResource $storeLocatorResource
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param ImageUploader $imageUploader
     * @param Context $context
     * @param Geolocation $geolocation
     */
    public function __construct(
        StoreLocatorResource $storeLocatorResource,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorRepository $storeLocatorRepository,
        ImageUploader $imageUploader,
        Context $context,
        Geolocation $geolocation
    ) {
        $this->imageUploader=$imageUploader;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
        $this->geolocation = $geolocation;
        $this->storeLocatorRepository= $storeLocatorRepository;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     * @throws AlreadyExistsExceptionAlias
     * @throws GuzzleException
     * @throws LocalizedException
     * @throws \JsonException
     */
    public function execute(): ResultInterface
    {
        $postData = $this->getRequest()->getParams();
        $model = $this->storeLocatorFactory->create();
        if ($postData['entity_id']) {
            $this->storeLocatorResource->load($model, $postData['entity_id']);
        } else {
            unset($postData['entity_id']);
        }
        $model->setData($postData);
        $this->imageUploader->setBasePath('store/image');
        $this->imageUploader->setBaseTmpPath('store/tmp/image');
        $this->setImage($postData, $model);
        $this->storeLocatorResource->save($model);
        $this->messageManager->addSuccessMessage('Store has been saved');
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setPath('*/*/index');
        return $result;
    }

    /**
     * @param $data
     * @param StoreLocator $model
     * @return StoreLocator
     * @throws LocalizedException
     */
    public function setImage($data, StoreLocator $model): StoreLocator
    {
        if (isset($data['image'][0]['name'], $data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $this->imageUploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = '';
        }
        $model->setImage($data['image']);
        return $model;
    }
}
