<?php

declare(strict_types=1);

namespace Tesl\StoreLocator\Controller;

use Tesl\StoreLocator\Api\StoreLocatorRepositoryInterface as StoreLocatorRepository;
use Tesl\StoreLocator\Model\CheckUrl;
use Tesl\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{

    /**
     * @param ActionFactory $actionFactory
     * @param StoreLocatorRepository $storeRepository
     * @param StoreLocatorResource $storeResource
     * @param CheckUrl $url
     */
    public function __construct(
        ActionFactory $actionFactory,
        StoreLocatorRepository $storeRepository,
        StoreLocatorResource $storeResource,
        CheckUrl $url,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
    ) {
        $this->actionFactory = $actionFactory;
        $this->storeRepository = $storeRepository;
        $this->storeResource = $storeResource;
        $this->url = $url;
        $this->_forwardFactory = $forwardFactory;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     * @throws \Exception
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $urlKey = trim($request->getPathInfo(), '/');
        $url = explode('/', $urlKey);

        if (str_contains($urlKey, 'stores')) {
            $request->setModuleName('stores');
            $request->setControllerName('store');
            $request->setActionName('index');

            if (isset($url[1])) {
                $storeUrl = $this->url->checkUrlKeys($url[1]);
                $store = $this->storeRepository->getById((int)$storeUrl);
                $request->setParams([
                    'store' => $store,
                ]);
            } else {
                return null;
            }
        }else{
            return null;
        }
        return $this->actionFactory->create(Forward::class);
    }
}
