<?php

namespace Tesl\StoreLocator\Plugin;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class UrlRewrite
{
    public function beforeSave(\Tesl\StoreLocator\Model\ResourceModel\StoreLocator $subject, \Tesl\StoreLocator\Model\StoreLocator $model)
    {
        return [$model];
    }
}
