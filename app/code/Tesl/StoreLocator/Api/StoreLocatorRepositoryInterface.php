<?php
namespace Tesl\StoreLocator\Api;

use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface;
use Tesl\StoreLocator\Api\Data\StoreLocatorSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface StoreLocatorRepositoryInterface
{
    /**
     * @param int $id
     * @return \Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface
     */
    public function get(int $id):StoreLocatorModelInterface;

    /**
     * @param int|null $store_id
     * @return StoreLocatorModelInterface
     */
    public function getById(int $store_id): StoreLocatorModelInterface;
    /**
     * @param  SearchCriteriaInterface $searchCriteria
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria):StoreLocatorSearchResultInterface;
    /**
     * @param StoreLocatorModelInterface $storeLocator
     * @return StoreLocatorModelInterface
     */
    public function save(StoreLocatorModelInterface $storeLocator):StoreLocatorModelInterface;

    /**
     * @param StoreLocatorModelInterface $storeLocator
     * @return bool
     */
    public function delete(StoreLocatorModelInterface $storeLocator):bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id):bool;
}
