<?php
namespace Tesl\StoreLocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreLocatorSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface[]
     */
    public function getItems():StoreLocatorModelInterface;

    /**
     * @param \Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
