<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Tesl\StoreLocator\Model\CheckUrl;

class UrlRewrites implements ObserverInterface
{
    /**
     * @param CheckUrl $url
     */
    public function __construct(
        CheckUrl $url
    ) {
        $this->url = $url;
    }

    /**
     * @param Observer $observer
     * @return array|mixed|void|null
     */
    public function execute(Observer $observer)
    {
        $store = $observer->getData('object');
        if ($store->getStoreName() === null) {
            return $store;
        }
        $name = str_replace(' ', '-', strtolower($store['store_name']));
        try {
            if (!empty($store['store_url_key'])) {
                if ($this->url->checkUniqueUrl($store['store_url_key'])) {
                    return $store;
                }
                $store->setUrl($name . '-' . 'store' .'-'. $store->getEntityId());
            } else {
                throw new \RuntimeException();
            }
        } catch (\Exception $e) {
            if ($this->url->checkUniqueUrl($name)) {
                $store->setUrl($name);
            } else {
                $store->setUrl($name . '-' . 'store' . '-'. $store->getEntityId());
            }
        }
        return $store;
    }
}
