<?php

namespace Tesl\StoreLocator\Observer;

use Magento\Framework\Exception\LocalizedException;
use Tesl\StoreLocator\Model\Geolocation;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CoordinatesSave implements ObserverInterface
{
    private Geolocation $geolocation;

    /**
     * @param Geolocation $geolocation
     */
    public function __construct(
        Geolocation $geolocation
    ) {
        $this->geolocation = $geolocation;
    }

    /**
     * @param Observer $observer
     * @return array|mixed|void|null
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $store = $observer->getData('object');
        $data = $store->getData();

        if(empty($data['longitude']) && empty($data['latitude'])){
            $coordinates = $this->geolocation->getCoordinates($data['address']);
            if(!$coordinates) {
                //throw new LocalizedException(__('Address has not found'));
                $store->setLatitude('0');
                $store->setLongitude('0');
                return $store;
            }
            $store->setLatitude((string)$coordinates['lat']);
            $store->setLongitude((string)$coordinates['lng']);
            return $store;
        }

        if(!empty($data['longitude']) && !empty($data['latitude'])){
            return $store;
        }

        throw new LocalizedException(__('Input longitude and latitude or do not input'));

    }
}
