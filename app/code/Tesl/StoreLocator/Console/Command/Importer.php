<?php
namespace Tesl\StoreLocator\Console\Command;

use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterfaceFactory as StoreLocatorFactory;
use Tesl\StoreLocator\Api\StoreLocatorRepositoryInterface as StoreLocatorRepository;
use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Csv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Importer extends Command
{

    /**
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param Csv $csv
     * @param StoreLocatorRepository $storeLocatorRepository
     */
    public function __construct(
        StoreLocatorFactory    $storeLocatorFactory,
        Csv                    $csv,
        StoreLocatorRepository $storeLocatorRepository,
    ) {
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->csv = $csv;
        $this->storeLocatorRepository = $storeLocatorRepository;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('tesl:import:storelocator');
        $this->setDescription('Import stores from csv to DB');
        $this->addOption('path', "p", InputOption::VALUE_OPTIONAL, "Path for csv file");
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws LocalizedException
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $path = $input->getOption('path');
        if (empty($path)) {
            throw  new \RuntimeException("Invalid argument");
        }
        try {
            $csvData = fopen($path, 'rb');
            $counter = 1;
            $keys = fgetcsv($csvData);
            while ($row = fgetcsv($csvData)) {
                $store = $this->storeLocatorFactory->create();
                $data = array_combine($keys, $row);
                $store->setStoreName($data['name']);
                $store->setAddress($data['country'] . ', ' . $data['city'] . ', ' . $data['address']);
                $store->setDescription($data['description']);
                $store->setImage($data['store_img']);
                $store->setUrl($data['url_key']);
                $this->storeLocatorRepository->save($store);
                unset($store);
                echo "Store $counter saved \n";
                $counter++;
            }
        } catch (Exception $e) {
            $output->writeln('Invalid CSV');
            $output->writeln($e->getMessage());
        }
    }
}
