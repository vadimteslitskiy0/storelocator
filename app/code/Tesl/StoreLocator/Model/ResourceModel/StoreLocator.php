<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface;
use Magento\Framework\DB\Select;

class StoreLocator extends AbstractDb
{
    public const ENTITY_TABLE_NAME = 'tesl_sl';
    /**
     *
     * @inheirtDoc
     */
    protected function _construct(): void
    {
        $this->_init(self::ENTITY_TABLE_NAME, 'entity_id');
    }

}
