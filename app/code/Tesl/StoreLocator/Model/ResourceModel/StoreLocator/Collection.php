<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Model\ResourceModel\StoreLocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct():void
    {
        parent::_construct();
        $this->_init(
            \Tesl\StoreLocator\Model\StoreLocator::class,
            \Tesl\StoreLocator\Model\ResourceModel\StoreLocator::class
        );
    }

}
