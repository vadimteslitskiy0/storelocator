<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Model;

use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Backend\App\Action\Context;

class Geolocation
{
    public const PATH_TO_API_KEY= 'tesl_store_locator/general/api_key';
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var mixed|string
     */
    private mixed $location;
    private ClientFactory $guzzleFactory;
    private Context $context;
    private \Magento\Framework\Message\ManagerInterface $messageManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ClientFactory $guzzleFactory
     * @param Context $context
     * @param array $location
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \GuzzleHttp\ClientFactory $guzzleFactory,
        Context $context,
        array $location = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->location=$location;
        $this->guzzleFactory= $guzzleFactory;
        $this->context= $context;
        $this->messageManager= $context->getMessageManager();
    }

    /**
     * @param string $address
     * @return array
     */
    public function getCoordinates(string $address) : array
    {
        try {
            $apiKey = $this->scopeConfig->getValue(self::PATH_TO_API_KEY, ScopeInterface::SCOPE_STORE);
            $http= $this->guzzleFactory->create();
            $response = $http->get('https://maps.googleapis.com/maps/api/geocode/json?address=' .
                    urlencode($address) . '&key=' . $apiKey);
            $geo = json_decode((string)$response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            $this->location = (array)$geo->results[0]->geometry->location;
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        return $this->location;
    }
}
