<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Model;

use Tesl\StoreLocator\Api\Data\StoreLocatorModelInterface;
use Tesl\StoreLocator\Model\ResourceModel\StoreLocator;
use Magento\Framework\DB\Select;

class CheckUrl
{
    public const ENTITY_TABLE_NAME = 'tesl_sl';

    /**
     * @param StoreLocator $storeResource
     */
    public function __construct(
        \Tesl\StoreLocator\Model\ResourceModel\StoreLocator $storeResource
    )
    {
        $this->storeResource = $storeResource;
    }

    /**
     * @param string $url
     * @return string|bool
     */
    public function checkUrlKeys(string $url): string|bool
    {
        $select = $this->loadByUrlKey($url);

        $select->reset(Select::COLUMNS)
            ->columns(StoreLocatorModelInterface::ENTITY_ID)
            ->limit(1);

        return $this->storeResource->getConnection()->fetchOne($select);
    }

    /**
     * @param string $url
     * @return Select
     */
    public function loadByUrlKey(string $url): Select
    {
        return $this->storeResource->getConnection()->select()
            ->from([self::ENTITY_TABLE_NAME])
            ->where(StoreLocatorModelInterface::STORE_URL_KEY . "= ?", $url);
    }

    /**
     * @param $url
     * @return bool
     */
    public function checkUniqueUrl($url): bool
    {
        $select = $this->storeResource->getConnection()->select()
            ->from(self::ENTITY_TABLE_NAME)
            ->where(StoreLocatorModelInterface::STORE_URL_KEY . '= ?', $url);
        if (!$this->storeResource->getConnection()->fetchOne($select)) {
            return true;
        }
        return false;
    }

}
