<?php
declare(strict_types=1);

namespace Tesl\StoreLocator\Model;

use Magento\Framework\AuthorizationInterface;

class Authorization
{
    public const ACTION_STORE_LOCATOR_EDIT = 'Tesl_StoreLocator::edit';

    public const ACTION_STORE_LOCATOR_DELETE = 'Tesl_StoreLocator::delete';

    /**
     * @var AuthorizationInterface
     */
    private AuthorizationInterface $authorization;

    /**
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
        AuthorizationInterface $authorization
    ) {
        $this->authorization= $authorization;
    }

    /**
     * Check if resource is available
     *
     * @param string $aclResource
     * @return bool
     */
    public function isAllowed(string $aclResource) :bool
    {
        return $this->authorization->isAllowed($aclResource);
    }

}
