<?php

declare(strict_types=1);

namespace Tesl\StoreLocator\Ui\Component\Form\Button;

use Tesl\StoreLocator\Model\Authorization;

class Save implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    /**
     * Get button configuration
     *
     * @return array
     */
    public function getButtonData(): array
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage_init' => [
                    'button' => [
                        'event' => 'save'
                    ],
                ],
                'form-role' => 'save'
            ],
            'aclResource' => Authorization::ACTION_STORE_LOCATOR_EDIT,
            'sort_order' => 10
        ];
    }
}
